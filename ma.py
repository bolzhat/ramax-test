# -*- coding: utf-8 -*-


class MyError(Exception):
    pass


class Parent(object):

    first = None

    def __init__(self, i=3):
        self.i = i

    def fnc(self, a, b=2):
        if a == 7:
            raise MyError("Error text")

        return a * b * self.i

    def isFirst(self):
        return self.first

    @property
    def isSecond(self):
        return None if self.first is None else not self.first


class First(object):

    first = True


class Second(object):

    first = False


class A(First, Parent):
    pass
